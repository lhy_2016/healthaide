//
//  HSAlertView.swift
//  home-school
//
//  Created by 张晓林 on 16/7/21.
//  Copyright © 2016年 张晓林. All rights reserved.
//

import UIKit

class HSAlertView: NSObject {
    
    /// 自定义底部弹出提示窗
    class func bottomAlertView(_ title: String, maxY: CGFloat = screenH/2 + 22) {
        // 初始化一个标签视图
        let alertL = UILabel(title: title, color: UIColor.white, fontSize: 17)
        alertL.frame = CGRect(x: 0, y: 0, width: 150, height: 44)
        alertL.center = CGPoint(x: screenW/2, y: maxY - 22)
        alertL.backgroundColor = UIColor.black
        alertL.alpha = 0.0
        alertL.textAlignment = .center
        alertL.layer.cornerRadius = 5
        alertL.layer.masksToBounds = true
        // 将视图添加到window上
        UIApplication.shared.keyWindow?.addSubview(alertL)
        // 动画
        UIView.animate(withDuration: 1.0, animations: {
            alertL.alpha = 1.0
            }, completion: { (_) in
                UIView.animate(withDuration: 1.0, animations: {
                    alertL.alpha = 0.0
                    }, completion: { (_) in
                        alertL.removeFromSuperview()
                })
        }) 
    }

}
