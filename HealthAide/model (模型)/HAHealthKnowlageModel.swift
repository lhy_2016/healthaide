//
//  HAHealthKnowlageModel.swift
//  HealthAide
//
//  Created by rimi on 16/11/7.
//  Copyright © 2016年 赖虹宇. All rights reserved.
//

import UIKit

class HAHealthKnowlageModel: NSObject {
    var ctime:String?
    var id:String?
    var intro:String?{
        didSet{
        computingCellHeight()
        }
    }
    var title:String?
    var url:String?
    var media_name:String?
    var cellHight:CGFloat = 0.0
    var img:String?{
        didSet{
            
        }
    }
    init(dict:[String:Any]) {
        super.init()
        //KVC方法赋值
        setValuesForKeys(dict)
    }
    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        
    }
    //MARK:-- 计算cell高度
    func computingCellHeight()  {
        cellHight = 0
        //加标题高度
        cellHight += 18.0 + margin
        let detailStr = intro ?? ""
        let detailstr = detailStr as NSString
        let detailHight  =  detailstr.boundingRect(with: CGSize(width: screenW - margin * 4, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 14)], context: nil).height
        //加详情高度
        cellHight += margin + detailHight
        let imgStr = img ?? ""
        if imgStr == "" {
            
        }else{
            //加图片高度
            cellHight += imageH + margin
        }
        //加来源高度
        cellHight += margin + 15 + margin
    }
    
}
