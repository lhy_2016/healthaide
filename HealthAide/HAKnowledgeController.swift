//
//  HAKnowledgeController.swift
//  HealthAide
//
//  Created by rimi on 16/11/8.
//  Copyright © 2016年 赖虹宇. All rights reserved.
//

import UIKit

class HAKnowledgeController: UIViewController {
    var id:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        view.backgroundColor = UIColor.white
       loadData()
    }
    func setupUI()  {
        
    }
    func loadData()  {
        let url = "http://route.showapi.com/90-88"
        let str = id ?? ""
        let prama = ["id":str]
        HANetWorkTools.shared.downloadData(urlStr: url, parameters: prama) {[weak self] (data, error) in
            guard let data = data else {
                return
            }
            let dict = data as! [String:Any]
            let showapi_res_code = dict["showapi_res_code"] as!  Int
            if showapi_res_code == 0 {
                let showapi_res_body = dict["showapi_res_body"] as! [String:Any]
                let item = showapi_res_body["item"] as! [String:Any]
                print(item)
                let content = item["content"] as! String
                let webView = UIWebView.init(frame:  CGRect(x: 0, y: 64, width: screenW, height: screenH - 64))
                self?.view.addSubview(webView)
                let str = content.replacingOccurrences(of: "\n", with: "<br/>")
                webView.loadHTMLString(str, baseURL: nil)
            }
        }
    }
 

}
