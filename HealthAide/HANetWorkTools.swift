//
//  HANetWorkTools.swift
//  HealthAide
//
//  Created by rimi on 16/11/7.
//  Copyright © 2016年 赖虹宇. All rights reserved.
//

import UIKit
import AFNetworking
class HANetWorkTools: AFHTTPSessionManager {
    ///创建单列类方法
    ///swift的单列写法
    
    static let shared : HANetWorkTools = {
        let instences = HANetWorkTools(baseURL: URL(string: ""))
        instences.responseSerializer.acceptableContentTypes?.insert("text/html")
        return instences
    }()
    func downloadData(urlStr:String,parameters:Any?,dataCallBack:@escaping (_ data:Any?,_ error:Error?) -> ())  {
        let prama = [appidKey:appid,secretKey:secret]
        let paramas = NSMutableDictionary.init(dictionary: prama)
        if parameters != nil {
            let dict = parameters as! [String:String]
            paramas.addEntries(from: dict)
        }
        get(urlStr, parameters: paramas, progress: nil, success: { (_, data) in
            //闭包回调数据
            dataCallBack(data,nil)
        }) { (_, error) in
            //闭包回调错误
            dataCallBack(nil,error)
        }
       
    }
}
