//
//  HASearchController.swift
//  
//
//  Created by rimi on 16/11/7.
//
//

import UIKit

class HASearchController: UIViewController ,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource{
    /// 懒加载
    private lazy var tableView:UITableView = {
        let  tableView = UITableView.init(frame: CGRect(x: 0, y: 64, width: screenW, height: screenH - 44 - 64), style: .plain)
        tableView.rowHeight = 200
        tableView.register(HAHealthHomeCell.self, forCellReuseIdentifier: "HAHealthHomeCell")
        return tableView
    }()
    var tid:String?
    var searthTF:UITextField?
    var dataArr:[HAHealthKnowlageModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 200
        addNavigationSearthTF()
    }
    //MARK:-- 添加导航栏搜索框和搜索按钮
    func addNavigationSearthTF()  {
        searthTF = UITextField.init(frame: CGRect(x: (screenW - screenW/1.5)/2, y: 5, width: screenW/1.5, height: 34))
        navigationController?.navigationBar.addSubview(searthTF!)
        searthTF?.placeholder = "搜索关键词"
        searthTF?.layer.cornerRadius = 17
        searthTF?.layer.masksToBounds = true
        searthTF?.backgroundColor = UIColor.white
        searthTF?.textAlignment = .center
        searthTF?.delegate = self
        let searchBtn = UIBarButtonItem.init(title: "搜索", style: .plain, target: self, action: #selector(searchBtnClicked))
        navigationItem.rightBarButtonItem = searchBtn
        
    }
    func searchBtnClicked()  {
        let text = searthTF?.text ?? ""
        if text == "" {
            HSAlertView.bottomAlertView("搜索不能为空")
            return
        }
        loadData()
    }
    //MARK:--输入框代理方法
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searthTF?.endEditing(true)
        searthTF?.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searthTF?.isHidden = false
        searthTF?.becomeFirstResponder()
        
    }
    //MARK:--数据请求
    func loadData()  {
        let url = "http://route.showapi.com/90-87"
        let text = searthTF?.text ?? ""
        let str = tid ?? ""
        let prama = ["tid":str,"key":text,"page":"1"]
        HANetWorkTools.shared.downloadData(urlStr: url, parameters: prama) { [weak self](data, error) in
            print(data)
            guard let data = data else{
                return
            }
            let dict = data as! [String:Any]
            let showapi_res_code = dict["showapi_res_code"] as? Int
            let code = showapi_res_code ?? -1
            if code == 0 {
                let showapi_res_body = dict["showapi_res_body"] as? [String:Any]
                guard let body = showapi_res_body else{
                return
                }
                let pagebean = body["pagebean"] as? [String:Any]
                guard let pagebeans = pagebean else{
                return
                }
                let contentlist = pagebeans["contentlist"] as? [[String:Any]]
                guard let contentArr = contentlist else{
                return
                }
                for aDict in contentArr {
                    let model = HAHealthKnowlageModel.init(dict: aDict)
                    self?.dataArr.append(model)
                }
            }
        }
        
    }
    //MARK:--表格视图代理方法
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HAHealthHomeCell", for: indexPath) as! HAHealthHomeCell
        cell.model = dataArr[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
}















