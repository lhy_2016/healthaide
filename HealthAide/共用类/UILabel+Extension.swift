//
//  UILabel+Extension.swift
//  HomeSchoolDemo
//
//  Created by 张晓林 on 16/10/6.
//  Copyright © 2016年 张晓林. All rights reserved.
//

import UIKit

extension UILabel {

    /// 便利构造函数
    ///
    /// - parameter title:    文字
    /// - parameter fontSize: 字体
    /// - parameter color:    颜色
    ///
    /// - returns: 标签
    convenience init(attributeTitle: NSAttributedString?, fontSize: CGFloat) {
        self.init()
        attributedText = attributeTitle
        font = UIFont.systemFont(ofSize: fontSize)
        textAlignment = .left
        numberOfLines = 0
    }
    
    
    /// 便利构造函数
    ///
    /// - parameter title:          title
    /// - parameter color:          color
    /// - parameter fontSize:       fontSize
    /// - parameter layoutWidth:    文本换行宽度，默认为0
    ///
    /// - returns: UILabel
    /// - 如果指定换行宽度，是左对齐，否则是居中对齐
    convenience init(title: String, color: UIColor, fontSize: CGFloat, layoutWidth: CGFloat = 0) {
        self.init()
        
        text = title
        textColor = color
        font = UIFont.systemFont(ofSize: fontSize)
        numberOfLines = 0
        
        if layoutWidth > 0 {
            // 指定多行文本的换行宽度
            preferredMaxLayoutWidth = layoutWidth
            textAlignment = .left
            //        } else {
            //            textAlignment = .Center
        }
        
        sizeToFit()
    }
}
