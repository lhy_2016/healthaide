//
//  HAHealthHomeCell.swift
//  HealthAide
//
//  Created by rimi on 16/11/7.
//  Copyright © 2016年 赖虹宇. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage
class HAHealthHomeCell: UITableViewCell {
    var titleL:UILabel?//标题
    var fromL : UILabel?//来源
    var urlBtn :UIButton?//网址来源
    var bgImage:UIImageView?//图片背景视图
    var detailL:UILabel?//部分详情
    var model:HAHealthKnowlageModel?{
        didSet{
            titleL?.text = model?.title
            fromL?.text = model?.media_name
            detailL?.text = model?.intro
            let imgstr = model?.img ?? ""
            if imgstr == "" {
                bgImage?.isHidden = true
                fromL?.snp.remakeConstraints({ (make) in
                    make.top.equalTo((detailL?.snp.bottom)!).offset(margin)
                    make.left.equalTo(titleL!)
                    make.right.equalTo(titleL!)
                })
            }else{
                bgImage?.isHidden = false
                fromL?.snp.remakeConstraints({ (make) in
                    make.top.equalTo((bgImage?.snp.bottom)!).offset(margin)
                    make.left.equalTo(titleL!)
                    make.right.equalTo(titleL!)
                })
                bgImage?.sd_setImage(with: URL.init(string: imgstr), placeholderImage: UIImage.init(named: ""))
                
            }
        }
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    func setupUI()  {
     
        
        
        titleL = UILabel()
        contentView.addSubview(titleL!)
        titleL?.snp.makeConstraints({ (make) in
            make.left.equalTo(margin * 2)
            make.right.equalTo(-margin * 2)
            make.top.equalTo(margin)
        })
        titleL?.font = UIFont.systemFont(ofSize: 16)
        detailL = UILabel()
        contentView.addSubview(detailL!)
        detailL?.snp.makeConstraints({ (make) in
            make.top.equalTo((titleL?.snp.bottom)!).offset(margin)
            make.right.equalTo(titleL!)
            make.left.equalTo(titleL!)
        })
        detailL?.numberOfLines = 0
        detailL?.font = UIFont.systemFont(ofSize: 14)
        detailL?.textColor = UIColor.gray
        bgImage = UIImageView()
       contentView.addSubview(bgImage!)
        bgImage?.snp.makeConstraints({ (make) in
            make.left.equalTo(titleL!).offset(imageW)
            make.width.equalTo(imageW)
            make.top.equalTo((detailL?.snp.bottom)!).offset(margin)
            make.height.equalTo(imageH)
        })
//        bgImage?.backgroundColor = UIColor.green
        fromL = UILabel()
        contentView.addSubview(fromL!)
        fromL?.snp.makeConstraints({ (make) in
            make.top.equalTo((bgImage?.snp.bottom)!).offset(margin)
            make.left.equalTo(titleL!)
            make.right.equalTo(titleL!)
        })
        fromL?.font = UIFont.systemFont(ofSize: 15)
        fromL?.textAlignment = .right
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
